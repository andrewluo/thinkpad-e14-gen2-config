# thinkpad-e14-gen2-config

Andrew Luo's Linux Configuration for **Thinkpad E14 Gen 2**

# GRUB Configuration
The Thinkpad E14 Gen 2 has an infurating backlight bug that prevents brightness from being adjusted. To fix this problem, add the following kernel parameters to your GRUB configuration in `/etc/default/grub`

```
GRUB_CMDLINE_LINUX="amdgpu.backlight=0 acpi_osi=Linux"
```
